import * as commonConst from "../model/CommonConstant";
import { MinePageItem } from "../model/MinePageItemData"
import mainViewModel from '../viewmodel/MainViewModel';
import router from '@ohos.router';
import AccountBean from '../model/AccountBean';

/**
 * “我的” tab content
 */
@Component
export default struct MineComponent {

  @Consume currentLoginAccount: AccountBean;

  @Builder
  settingCell(item: MinePageItem) {
    Row() {
      Row({ space: commonConst.COMMON_SPACE }) {
        Image(item.img)
          .width($r('app.float.setting_size'))
          .height($r('app.float.setting_size'))
        Text(item.title)
          .fontSize($r('app.float.normal_text_size'))
      }

      if (item.others === null) {
        Image($r('app.media.right_grey'))
          .width($r('app.float.setting_jump_width'))
          .height($r('app.float.setting_jump_height'))
      } else {
        Toggle({ type: ToggleType.Switch, isOn: false })
      }
    }
    .justifyContent(FlexAlign.SpaceBetween)
    .width(commonConst.FULL_PARENT)
    .padding({
      left: $r('app.float.setting_settingCell_left'),
      right: $r('app.float.setting_settingCell_right')
    })
  }

  build() {
    Scroll() {
      Column({ space: commonConst.COMMON_SPACE }) {

        Row() {
          // 头像，暂时写死
          Image($r('app.media.yilian_app_icon'))
            .width($r('app.float.setting_account_size'))
            .height($r('app.float.setting_account_size'))
          Column() {
            Text(this.currentLoginAccount.username)
              .fontSize($r('app.float.setting_account_fontSize'))
            Text(this.currentLoginAccount.email)
              .fontSize($r('app.float.little_text_size'))
              .margin({ top: $r('app.float.setting_name_margin') })
          }
          .alignItems(HorizontalAlign.Start)
          .margin({ left: $r('app.float.setting_account_margin') })
        }
        .margin({ top: $r('app.float.setting_account_margin') })
        .alignItems(VerticalAlign.Center)
        .width(commonConst.FULL_PARENT)
        .height($r('app.float.setting_account_height'))
        .backgroundColor(Color.White)
        .padding({ left: $r('app.float.setting_account_padding') })
        .borderRadius($r('app.float.setting_account_borderRadius'))

        List() {
          ForEach(mainViewModel.getSettingListData(), (item: MinePageItem) => {
            ListItem() {
              this.settingCell(item)
            }
            .height($r('app.float.setting_list_height'))
          }, item => JSON.stringify(item))
        }
        .backgroundColor(Color.White)
        .divider({
          strokeWidth: $r('app.float.setting_list_strokeWidth'),
          color: Color.Grey,
          startMargin: $r('app.float.setting_list_startMargin'),
          endMargin: $r('app.float.setting_list_endMargin')
        })
        .borderRadius($r('app.float.setting_list_borderRadius'))
        .padding({ top: $r('app.float.setting_list_padding'), bottom: $r('app.float.setting_list_padding') })

        Blank()

        Button($r('app.string.setting_button'), { type: ButtonType.Capsule })
          .width(commonConst.BUTTON_WIDTH)
          .height($r('app.float.login_button_height'))
          .fontSize($r('app.float.normal_text_size'))
          .fontColor($r('app.color.setting_button_fontColor'))
          .fontWeight(FontWeight.Medium)
          .backgroundColor($r('app.color.setting_button_backgroundColor'))
          .margin({ bottom: $r('app.float.setting_button_bottom') })
          .onClick(() => {
            router.replaceUrl({ url: "pages/LoginPage"});
          })
      }
      .height(commonConst.FULL_PARENT)
    }
  }
}



