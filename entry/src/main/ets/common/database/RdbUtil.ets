import data_rdb from '@ohos.data.rdb';
import * as CommonConstants from '../../model/CommonConstant';
import { Logger } from '../logs/Logger';

export default class RdbUtil {
  private rdbStore: any = null;
  private tableName: string;
  private sqlCreateTable: string;
  private initSql: string;
  private columns: Array<string>;

  constructor(tableName: string, sqlCreateTable: string, columns: Array<string>, initSql: string = '') {
    this.tableName = tableName;
    this.sqlCreateTable = sqlCreateTable;
    this.columns = columns;
    this.initSql = initSql;
  }

  getRdbStore(callback) {
    if (!callback || typeof callback == 'undefined' || callback == undefined) {
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'getRdbStore() has no callback!');
      return;
    }
    if (this.rdbStore != null) {
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'The rdbStore exists.');
      callback();
      return
    }
    let context = getContext(this);
    data_rdb.getRdbStore(context, CommonConstants.STORE_CONFIG, 1, (err, rdb) => {
      if (err) {
        Logger.error(`${CommonConstants.RDB_TAG}`, 'gerRdbStore() failed, err: ' + err);
        return;
      }
      this.rdbStore = rdb;
      this.rdbStore.executeSql(this.sqlCreateTable);
      if (this.initSql != '') {
        this.rdbStore.executeSql(this.initSql)
      }
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'getRdbStore() finished.');
      callback();
    });
  }

  insertData(data, callback) {
    if (!callback || typeof callback == 'undefined' || callback == undefined) {
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'insertData() has no callback!');
      return;
    }
    let resFlag: boolean = false;
    const valueBucket = data;
    this.rdbStore.insert(this.tableName, valueBucket, function (err, ret) {
      if (err) {
        Logger.error(`${CommonConstants.RDB_TAG}`, 'insertData() failed, err: ' + JSON.stringify(err));
        callback(resFlag);
        return;
      }
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'insertData() finished: ' + ret);
      callback(ret);
    });
  }

  deleteData(predicates, callback) {
    if (!callback || typeof callback == 'undefined' || callback == undefined) {
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'deleteData() has no callback!');
      return;
    }
    let resFlag: boolean = false;
    this.rdbStore.delete(predicates, function (err, ret) {
      if (err) {
        Logger.error(`${CommonConstants.RDB_TAG}`, 'deleteData() failed, err: ' + err);
        callback(resFlag);
        return;
      }
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'deleteData() finished: ' + ret);
      callback(!resFlag);
    });
  }

  updateData(predicates, data, callback) {
    if (!callback || typeof callback == 'undefined' || callback == undefined) {
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'updateDate() has no callback!');
      return;
    }
    let resFlag: boolean = false;
    const valueBucket = data;
    this.rdbStore.update(valueBucket, predicates, function (err, ret) {
      if (err) {
        Logger.error(`${CommonConstants.RDB_TAG}`, 'updateData() failed, err: ' + err);
        callback(resFlag);
        return;
      }
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'updateData() finished: ' + ret);
      callback(!resFlag);
    });
  }

  query(predicates, callback) {
    if (!callback || typeof callback == 'undefined' || callback == undefined) {
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'query() has no callback!');
      return;
    }
    this.rdbStore.query(predicates, this.columns, function (err, resultSet) {
      if (err) {
        Logger.error(`${CommonConstants.RDB_TAG}`, 'query() failed, err: ' + err);
        return;
      }
      Logger.verbose(`${CommonConstants.RDB_TAG}`, 'query() finished.');
      callback(resultSet);
      resultSet.close();
    });
  }
}